package com.example.scannerprojectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.scannerprojectfinal.Adapter.CartListAdapter;
import com.example.scannerprojectfinal.Adapter.HistoryListAdapter;
import com.example.scannerprojectfinal.Database.SQLiteHelper;
import com.example.scannerprojectfinal.Model.Cart;
import com.example.scannerprojectfinal.Model.History;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;


public class HistoryActivity extends AppCompatActivity {

    //Display Listview
    public static SQLiteHelper sqLiteHelper;
    ListView listView;
    ArrayList<History> list = new ArrayList<History>();
    HistoryListAdapter adapter = null;
    SQLiteHelper helper;

    TextView TotalPenjualan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        TotalPenjualan = findViewById(R.id.totalPenjualan);

        listView = (ListView) findViewById(R.id.listView);
        int totalPrice = 0;

        list = new ArrayList<>();
        adapter = new HistoryListAdapter(this, R.layout.history_item, list);
        listView.setAdapter(adapter);

        helper = new SQLiteHelper(this, "HistoryDB.sqlite", null, 1);
        // get all data from sqlite
        Cursor cursor = helper.getData("SELECT ID, NAMA, ALAMAT, TLP, TOTAL, TANGGAL FROM HISTORY");
        Cursor cursor1 = helper.getData("SELECT * FROM HISTORY");
        list.clear();

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String nama = cursor.getString(1);
                String alamat = cursor.getString(2);
                String tlp = cursor.getString(3);
                String total = cursor.getString(4);
                String tanggal = cursor.getString(5);
                Log.e("total: ", total);
                totalPrice = totalPrice + Integer.parseInt(total);
                Log.e("pricetotal: ", String.valueOf(total));
                list.add(new History(id, nama, alamat, tlp, total, tanggal));
            } while (cursor.moveToNext());
        }

        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        TotalPenjualan.setText(formatRupiah.format((double)totalPrice));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(HistoryActivity.this);
                final int pos = position;
                builder.setTitle("Dialog Hapus")
                        .setMessage("Apakah anda ingin menghapus item ini ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.e("HISTORY ID =", list.get(pos).toString());
                                History history = list.get(pos);
                                helper.deleteDataHistory(history.getId());
                                list.remove(pos);
                                adapter.notifyDataSetChanged();
                                listView.invalidateViews();
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(getIntent());
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });
                builder.create();
                builder.show();
            }
        });
    }
}
