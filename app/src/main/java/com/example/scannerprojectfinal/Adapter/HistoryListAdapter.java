package com.example.scannerprojectfinal.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.scannerprojectfinal.Model.History;
import com.example.scannerprojectfinal.R;

import java.util.ArrayList;

public class HistoryListAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<History> historyList;

    public HistoryListAdapter(Context context, int layout, ArrayList<History> historyListList) {
        this.context = context;
        this.layout = layout;
        this.historyList = historyListList;


    }
    @Override
    public int getCount() {
        return historyList.size();
    }
    @Override
    public Object getItem(int position) {
        return historyList.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        TextView txtNama, txtAlamat, txtTlp, txtTotal, txtTanggal;
    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row = view;
        ViewHolder holder = new ViewHolder();

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.txtNama = (TextView) row.findViewById(R.id.txtNama);
            holder.txtAlamat = (TextView) row.findViewById(R.id.txtAlamat);
            holder.txtTlp = (TextView) row.findViewById(R.id.txtTlp);
            holder.txtTotal = (TextView) row.findViewById(R.id.txtTotal);
            holder.txtTanggal= (TextView) row.findViewById(R.id.txtTanggal);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }
        History history = historyList.get(position);
        holder.txtNama.setText(history.getNama());
        holder.txtAlamat.setText(history.getAlamat());
        holder.txtTlp.setText(history.getTlp());
        holder.txtTotal.setText(history.getTotal());
        holder.txtTanggal.setText(history.getTanggal());

        return row; }

}
