package com.example.scannerprojectfinal.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class SQLiteHelper extends SQLiteOpenHelper {

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void queryData(String sql){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

//    Database Scanner
//    DIbutuhkan untuk menyimpan data hasil scanner kedalam list array pada database CART

    public void insertData(String name, String quantity, String price){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO CART(NAME, QUANTITY, PRICE) VALUES (?, ?, ?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, name);
        statement.bindString(2, quantity);
        statement.bindString(3, price);
        statement.executeInsert();
    }
    public void updateData(String name, String quantity, String price, int id) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "UPDATE CART SET name = ?, quantity = ?, price = ? WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.bindString(1, name);
        statement.bindString(2, quantity);
        statement.bindString(3, price);
        statement.execute();
        database.close();
    }
    public  void deleteAllData() {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "DELETE FROM CART";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.execute();
        database.close();
    }
    public  void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "DELETE FROM CART WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);
        statement.execute();
        database.close();
    }
    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    //    Database History
    //    DIbutuhkan untuk menyimpan data hasil scanner kedalam list array pada database History

    public void insertDataHistory(String nama, String alamat, String tlp, String total, String tanggal){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO HISTORY(NAMA, ALAMAT, TLP, TOTAL, TANGGAL) VALUES (?, ?, ?, ?, ?)";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1, nama);
        statement.bindString(2, alamat);
        statement.bindString(3, tlp);
        statement.bindString(4, total);
        statement.bindString(5, tanggal);
        statement.executeInsert();
    }
    public  void deleteDataHistory(int id) {
        SQLiteDatabase database = getWritableDatabase();
        String sql = "DELETE FROM HISTORY WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);
        statement.execute();
        database.close();
    }
}
