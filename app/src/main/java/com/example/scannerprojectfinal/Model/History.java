package com.example.scannerprojectfinal.Model;

public class History {
    private int id;
    private String nama;
    private String alamat;
    private String tlp;
    private String total;
    private String tanggal;

    public History(int id, String nama, String alamat, String tlp, String total, String tanggal) {
        this.id = id;
        this.nama = nama;
        this.alamat = alamat;
        this.tlp = tlp;
        this.total = total;
        this.tanggal = tanggal;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTlp() {
        return tlp;
    }
    public void setTlp(String tlp) {
        this.tlp = tlp;
    }

    public String getTotal() {
        return total;
    }
    public void setTotal(String total) {
        this.total = total;
    }

    public String getTanggal() {
        return tanggal;
    }
    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

}