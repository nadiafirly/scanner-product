package com.example.scannerprojectfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.PermissionRequest;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.blikoon.qrcodescanner.QrCodeActivity;
import com.example.scannerprojectfinal.Adapter.CartListAdapter;
import com.example.scannerprojectfinal.Database.SQLiteHelper;
import com.example.scannerprojectfinal.Model.Cart;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.single.PermissionListener;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CartListActivity extends AppCompatActivity {

    //Display Listview
    public static SQLiteHelper sqLiteHelper;
    ListView listView;
    TextView totalPricetv;
    ArrayList<Cart> list = new ArrayList<Cart>();
    CartListAdapter adapter = null;
    SQLiteHelper helper;

    //Scanner
    private int mAmount = 0;
    int hargaTotal = 0;
    ImageButton scan;
    private static final int REQUEST_CODE_QR_SCAN = 101;

    //Slide ViewFlipper
    ViewFlipper v_flipper;

    //SavetoHistory
    Button buttonPesan;

    //show History
    public static SQLiteHelper historysqLiteHelper;
    SQLiteHelper historyhelper;
    ImageButton showHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);

        //********Scanner********
        scan = findViewById(R.id.scan);
        sqLiteHelper = new SQLiteHelper(this, "FoodDB.sqlite", null, 1);
        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS CART(Id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, quantity INTEGER, price INTEGER)");

        historysqLiteHelper = new SQLiteHelper(this, "HistoryDB.sqlite", null, 1);
        historysqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS HISTORY(Id INTEGER PRIMARY KEY AUTOINCREMENT, nama VARCHAR, alamat VARCHAR, tlp VARCHAR, total INTEGER, tanggal VARCHAR)");

        scan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Dexter.withContext(getApplicationContext())
                        .withPermission(Manifest.permission.CAMERA)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                                Intent i = new Intent(CartListActivity.this, QrCodeActivity.class);
                                startActivityForResult(i, REQUEST_CODE_QR_SCAN);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                                permissionDeniedResponse.getRequestedPermission();
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(com.karumi.dexter.listener.PermissionRequest permissionRequest, PermissionToken permissionToken) {

                            }
                        }).check();
            }
        });


        //********Display ListView********
        listView = (ListView) findViewById(R.id.listView);
        totalPricetv = findViewById(R.id.total);
        int total = 0;
        String totalPrice = null;

        list = new ArrayList<>();
        adapter = new CartListAdapter(this, R.layout.cart_item, list);
        listView.setAdapter(adapter);

        helper = new SQLiteHelper(this, "FoodDB.sqlite", null, 1);
        // get all data from sqlite
        Cursor cursor = helper.getData("SELECT ID, NAME, QUANTITY, PRICE FROM CART");
        Cursor cursor1 = helper.getData("SELECT * FROM CART");
        list.clear();

        if (cursor.moveToFirst()) {
            do {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String quantity = cursor.getString(2);
                String price = cursor.getString(3);
                Log.e("price: ", price);
                total = total + Integer.parseInt(price);
                Log.e("pricetotal: ", String.valueOf(total));
                list.add(new Cart(id, name, quantity, price));
            } while (cursor.moveToNext());
        }

//        Locale localeID = new Locale("in", "ID");
//        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
//
//        totalPricetv.setText(formatRupiah.format((double)total));
        totalPricetv.setText(String.valueOf(total));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CartListActivity.this);
                final int pos = position;
                builder.setTitle("Dialog Hapus")
                        .setMessage("Apakah anda ingin menghapus item ini ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.e("CART ID =", list.get(pos).toString());
                                Cart cart = list.get(pos);
                                helper.deleteData(cart.getId());
                                list.remove(pos);
                                adapter.notifyDataSetChanged();
                                listView.invalidateViews();
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(getIntent());
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });
                builder.create();
                builder.show();
            }
        });

        //ViewFlipper
        int images[] = {R.drawable.slide1, R.drawable.slide2, R.drawable.slide3, R.drawable.slide4,
                R.drawable.slide5};
        v_flipper = findViewById(R.id.v_flipper);
        for (int image : images) {
            flipperImages(image);
        }

        //********Save to History********
        buttonPesan = findViewById(R.id.button_pesan);
        buttonPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonPesan();
            }
        });

        showHistory = findViewById(R.id.showHistory);
        showHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CartListActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
        
    }

    private void buttonPesan() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(CartListActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.input_customer, null);
        final EditText mNamaPemesan = (EditText) mView.findViewById(R.id.hist_edt_nama_pemesan);
        final EditText mAlamat = (EditText) mView.findViewById(R.id.hist_edt_alamat_pemesan);
        final EditText mTlp = (EditText) mView.findViewById(R.id.hist_edt_tlp_pemesan);
        final TextView mTotal = (TextView) mView.findViewById(R.id.hist_txt_total_belanja);
        final TextView mTanggal = (TextView) mView.findViewById(R.id.hist_tanggal);

        //Dapetin Total Belanja
        String input = totalPricetv.getText().toString();
        mTotal.setText(input);

        //Dapetin Tanggal
        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM yyyy hh:mm:ss");//formating according to my need
        String date = formatter.format(today);
        mTanggal.setText(date);

        mBuilder.setPositiveButton("SELESAI PESAN",
                new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        if(Integer.parseInt(mTotal.getText().toString()) < 0 ){
                            Toast.makeText(CartListActivity.this, "Maaf Belum ada barang di keranjang",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.i("TXT PRICE",mNamaPemesan.getText().toString());
                        historysqLiteHelper.insertDataHistory(
                                mNamaPemesan.getText().toString().trim(),
                                mAlamat.getText().toString().trim(),
                                mTlp.getText().toString().trim(),
                                mTotal.getText().toString().trim(),
                                mTanggal.getText().toString().trim()
                        );
                        helper.deleteAllData();
                        list.clear();
                        adapter.notifyDataSetChanged();
                        listView.invalidateViews();
                        totalPricetv.setText(null);
                        Toast.makeText(getApplicationContext(), "Masuk History!", Toast.LENGTH_SHORT).show();

                    }
                });

        mBuilder.setView(mView);
        AlertDialog dialog = mBuilder.create();
        dialog.show();
    }



    //MethodViewFlipper
    private void flipperImages(int image) {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        v_flipper.addView(imageView);
        v_flipper.setFlipInterval(3000); //3 scn
        v_flipper.setAutoStart(true);
        //animationn
        v_flipper.setInAnimation(this, android.R.anim.slide_in_left);
        v_flipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    //Get Data QRCode
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if (result != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(CartListActivity.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if (requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            String[] kata = result.split(",");

            String nama_rp = kata[0];
            String harga_rp = kata[1];

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(CartListActivity.this);
            View mView = getLayoutInflater().inflate(R.layout.dialog_order, null);
            final EditText mNameProduct = (EditText) mView.findViewById(R.id.edt_product_name);
            final EditText mPrice = (EditText) mView.findViewById(R.id.edt_price);
            final EditText mQty = (EditText) mView.findViewById(R.id.edt_qty);
            final EditText mQtyD = (EditText) mView.findViewById(R.id.edt_qty_display);
            final EditText mTotal = (EditText) mView.findViewById(R.id.edt_total);
            final Button buttonIncrease = (Button) mView.findViewById(R.id.button_increase);
            final Button buttonDecrease = (Button) mView.findViewById(R.id.button_decrease);

            mNameProduct.append(nama_rp);
            mPrice.append(harga_rp);

            buttonIncrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mAmount++;
                    hargaTotal = mAmount * Integer.parseInt(mPrice.getText().toString());
                    mQtyD.setText(String.valueOf(mAmount));
                    mQty.setText(String.valueOf(mAmount));
                    mTotal.setText(String.valueOf(hargaTotal));
                }
            });

            buttonDecrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mAmount > 0) {
                        mAmount--;
                        hargaTotal = mAmount * Integer.parseInt(mPrice.getText().toString());
                        mQtyD.setText(String.valueOf(mAmount));
                        mQty.setText(String.valueOf(mAmount));
                        mTotal.setText(String.valueOf(hargaTotal));
                    }
                }
            });

            mBuilder.setPositiveButton("OK",
            new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which){
                    if(Integer.parseInt(mQty.getText().toString()) < 1 ){
                        Toast.makeText(CartListActivity.this, "Maaf Anda tidak dapat memesan kurang dari 1",Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Log.i("TXT PRICE",mNameProduct.getText().toString());
                    sqLiteHelper.insertData(
                            mNameProduct.getText().toString().trim(),
                            mQty.getText().toString().trim(),
                            mTotal.getText().toString().trim()
                    );
                    Toast.makeText(getApplicationContext(), "Masuk Keranjang!", Toast.LENGTH_SHORT).show();
                    mNameProduct.getText().clear();
                    mQty.getText().clear();
                    mQtyD.getText().clear();
                    mTotal.getText().clear();
                    dialog.cancel();
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                }
            });

            mBuilder.setView(mView);
            AlertDialog dialog = mBuilder.create();
            dialog.show();

        }
    }


}

